#!/bin/bash
# This script reads WORDPRESS_env and creates equivalent kubernetes secrets.
# It needs to be capable for creating k8s secrets by reading ENV variables as well,
#   as, that is the case with CI systems.
if [ ! -f ./kubernetes/wordpress/WORDPRESS_env ]; then
  echo "Could not find ENV variables file for WORDPRESS - ./kubernetes/wordpress/WORDPRESS_env"
  exit 1
fi

echo "First delete the old secret: WORDPRESS-credentials"
kubectl delete secret wordpress-credentials  || true

echo "Found WORDPRESS_env file, creating kubernetes secret: WORDPRESS-credentials"
source ./kubernetes/wordpress/WORDPRESS_env


kubectl create secret generic wordpress-credentials \
  --from-literal=WORDPRESS_DATABASE_PASSWORD=${WORDPRESS_DATABASE_PASSWORD} \
  --from-literal=WORDPRESS_DATABASE_NAME=${WORDPRESS_DATABASE_NAME} \
  --from-literal=WORDPRESS_DATABASE_USER=${WORDPRESS_DATABASE_USER} \
  --from-literal=WORDPRESS_DATABASE_HOST=${WORDPRESS_DATABASE_HOST}
