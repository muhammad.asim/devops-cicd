#!/bin/bash

# https://docs.aws.amazon.com/cli/latest/reference/kms/create-alias.html

echo -e "\nBest is to create the KMS Key Manually, because I foud the fields do change & behave differently because of A/c "string" length, so this is a demo, that is why I am using it.\n"


ACCOUNT_ID=`aws sts get-caller-identity | grep -i "account" | cut -d':' -d'"' -f4`
#KMS_KEY=`aws kms create-key | grep "key" | awk '{print $3}' | cut -f6 -d":" | cut -d"/" -f2`
ALIAS_NAME="jenkins"

KMS_KEY=`aws kms create-key | grep "key" `

echo -e "\nYour AWS A/c ID is "$ACCOUNT_ID"\n"

echo "$KMS_KEY"
echo -e 'aws kms create-alias --alias-name alias/YourAlias --target-key-id CopyTheTargetKMSkeyFromAbove&PasteHere'
echo 'aws kms list-aliases | grep "YourAlias"'



#END
