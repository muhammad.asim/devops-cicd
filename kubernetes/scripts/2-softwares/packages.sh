#!/bin/bash
#OS:Amazon-Linux+Ubuntu
#Prerequisite:Do Not Attach an IAM Role, use YOUR Account Progmatic Secret Key & Access Key
#Owner: Muhammad Asim ---> quickbooks2018@gmail.com


# Editor Insdie jenkins Container

apt-get update -y

apt-get install -y vim

apt-get install -y nano

apt-get install -y apt-utils
# Terraform Setup

apt-get update -y 2>&1 >/dev/null && apt-get install -y curl 2>&1 >/dev/null
curl -# -LO https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip
apt-get update -y 2>&1 >/dev/null && apt install -y unzip 2>&1 >/dev/null
unzip terraform_0.12.24_linux_amd64.zip 2>&1 >/dev/null
rm -rf *.zip  2>&1 >/dev/null
cp terraform /usr/bin/  2>&1 >/dev/null
cp terraform /usr/local/bin  2>&1 >/dev/null
rm -rf terraform



# Install AWS Cli

apt-get update -y  2>&1 >/dev/null
apt-get  install awscli  -y   2>&1 >/dev/null
apt-get install python3-pip -y   2>&1 >/dev/null
pip3 install --upgrade --user awscli  2>&1 >/dev/null




# Kubectl Installation
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl

chmod +x ./kubectl

mv ./kubectl /usr/bin/kubectl


# Eksctl Installation

curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /usr/local/bin
eksctl version

