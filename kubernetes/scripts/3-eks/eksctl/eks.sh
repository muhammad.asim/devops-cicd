#!/bin/bash

mkdir /root/.ssh 2>&1 >/dev/null

aws ec2 create-key-pair --key-name cloudelligent-eks --query 'KeyMaterial' --output text > /root/.ssh/cloudelligent.pem


#END



# EKS Setup

eksctl create cluster \
--name cloudelligent-eks \
--version 1.15 \
--region us-east-1 \
--vpc-cidr 10.11.0.0/16 \
--node-private-networking \
--nodegroup-name workers \
--node-type t2.medium \
--nodes 2 \
--nodes-min 1 \
--nodes-max 4 \
--ssh-access \
--zones us-east-1a,us-east-1b,us-east-1c \
--ssh-public-key cloudelligent-eks
